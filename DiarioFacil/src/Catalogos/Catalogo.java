/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Catalogos;

import Cliente.User;
import static DAO.DAO.connect;
import Observer.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class Catalogo {

    /**
     * @see I could have used the Singleton pattern for everyone to reference a single instance of the same Catalogue; 
     * but nowadays, catalogues are so customized to everyone's preferences that hardly we all look at the same catalogue.
     * 
     */
    public Catalogo() {
        read();
    }

    public void read() {
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM products ORDER BY id;";
//SELECT categories.name FROM categories,products where products.category = categories.id; 
        StringBuilder sb = new StringBuilder();
        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery(sql);
            while (rs.next()) {
                sb.append("id: " + rs.getInt("id") + "name: " + rs.getString("name") + "price: " + rs.getDouble("price") + "quantity: " + rs.getInt("quantity") + "\n\n");
//                sb.append("=================================\n");
//                sb.append("id" + "\t" + "name" + "\t" + "price" + "\t" + "quantity\n");
//                sb.append(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getDouble("price") + "\t" + rs.getInt("quantity") + "\n");
//                sb.append("=================================\n");
            }
            System.out.println(sb);
        } catch (SQLException e) {
            System.out.println("Error: Clase Catalogo, metodo listProducts");
            e.printStackTrace();
        }
        
    }
}

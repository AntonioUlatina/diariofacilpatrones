/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.Scanner;

/**
 * 
 * @author Administrator
 */
public class ConfigLoader {

    private Scanner scan;

    public String[] readConfigDB() {
        try {
            scan = new Scanner(getClass().getResourceAsStream("/Config/DB.cfg"));
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            System.err.println("Could not find \"/config/DB.cfg\".");
        }
        return new String[]{scan.nextLine(), scan.nextLine(), scan.nextLine()};
    }

}

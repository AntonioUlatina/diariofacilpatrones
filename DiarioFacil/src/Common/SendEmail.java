/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Administrator
 * 
 */
public class SendEmail {

    String host = "smtp.gmail.com";
    String user = "pregunticulatina@gmail.com";
    String pass = "S@nP3dr0";
    String from = "pregunticulatina@gmail.com";
    String subject;
    String message;

    public void sendTo(String subject, String message, String... emails) {
        try {

            //subject = "Netbeans test";
            //message = "Antonio was here!";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = new InternetAddress[emails.length];
            for (int i = 0; i < emails.length; i++) {
                address[i] = new InternetAddress(emails[i]);
                System.out.println("Email was sent to: " + address[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(message);

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            System.out.println("Email sent successfully");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * 
     * Usage: Write your message to let your provider know what quantity of product you wanted. You just have to write the message without any trailing whitespace and the required arguments; this method will take care of the rest.
     * @param subject
     * @param message
     * @param email
     * @param quantity 
     */
    public void sendOrderRequest(String subject, String message, int quantity, String... email) {
        sendTo(subject, message + ": " + quantity, email);
    }
    
    public void sendReceiptToClient(String message, String email){
        sendTo("Thank you for buying in DiarioFacil.com", message, email);
    }
    
/*
    public static void main(String args[]) {
        //System.out.println(new SendEmail().generateRandomCode());
        //new SendEmail().sendTo("Que pereza","Alguien tiene un dolar?", "lu.fer851@hotmail.com","diego.alfaro6@ulatina.net","teosmarin@hotmail.com");
        User antonio = new User("antonio.alvarez1@ulatina.net");
        User luis = new User("lu.fer851@hotmail.com");
        User diego = new User("diego.alfaro6@ulatina.net");
        User mateo = new User("teosmarin@hotmail.com");
        User rodiney = new User("sagra.0422@gmail.com");
        try {
            //new SendEmail().sendVerificationCodeTo(antonio, luis, diego, mateo);
            //new SendEmail().sendVerificationCodeTo(rodiney);
            new SendEmail().disclaimUserTerms();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
*/

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

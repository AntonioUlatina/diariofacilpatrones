/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author Pavilion
 */
import Observer.Product;
import java.util.ArrayList;
import java.util.Collection;

public abstract class IDealDecorator extends Product {

    public final Product product;
    public final Collection<IDealDecorator> dealList;
    public final Collection<IDealDecorator> comboList;
    public final Collection<Product> productList;

    {
        dealList = new ArrayList();
        comboList = new ArrayList();
        productList = new ArrayList();
    }

    public IDealDecorator(Product newProduct) {
        this.product = newProduct;
    }
    
    @Override
    public String getName(){
        return this.product.getName();
    }
    
    @Override
    public void setName(String name) {
        this.product.setName(name);
    }
    
    @Override
    public double getPrice(){
        return this.product.getPrice();
    }
    
    @Override
    public int getQuantity(){
        return this.product.getQuantity();
    }
    
}

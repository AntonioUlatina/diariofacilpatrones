/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

import Observer.Product;
import java.util.List;

/**
 *
 * @author Pavilion
 */
public class ComboDecorator extends IDealDecorator {

    private int id;
    private List<Product> products;

    public ComboDecorator(Product newProduct, List<Product> products) {
        super(newProduct);
        this.products = products;
    }

    @Override
    public double getPrice() {
        double price = 0;

        for (Product p : products) {
            price += p.getPrice();
        }

        return price * 0.9;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        String string = "";
        
        string += "\nID: " + id + "\nNombre: " + getName() + "\n";
        
        for (Product p : products) {
            string += p.getName()+ "\n";
        }
        
        return string;
    }
    
}

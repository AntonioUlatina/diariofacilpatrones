/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

import Observer.Product;

/**
 *
 * @author Administrator
 */
public class Item extends Product{
    public Product product;
    private int id;
    private int quantity;
    
    public Item(int id, Product product, int quantity){
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public void setQuantity(int quantity) {
        super.setQuantity(quantity); //To change body of generated methods, choose Tools | Templates.
    }

    public Product getProduct() {
        return product;
    }

    @Override
    public int getQuantity() {
        return super.getQuantity(); //To change body of generated methods, choose Tools | Templates.
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

import Factory.ProductFactory;
import Observer.Product;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Pavilion
 */
public class GetNFreeDecorator extends IDealDecorator{

    public static Collection<Collection<Product>> allThePromos = new ArrayList();
    public Collection<Product> promoProducts = new ArrayList();
    public String description;
    public String name;
    public double specialPrice;
    
    public GetNFreeDecorator(Product newProduct) {
        super(newProduct);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public double getPrice(int quantity) {
        return product.getPrice() * quantity;
    }
     
    public String productName(){
        return this.product.getName();
    }
    
    @Override
    public double getPrice(){
        return this.product.getPrice();
    }
    
    @Override
    public int getQuantity(){
        return this.product.getQuantity();
    }
 
    public int productQuantity(){
        return this.promoProducts.size();
    }

    public double getSpecialPrice() {
        return specialPrice;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    /**
     * @see Buy quantity. Get freeProduct free! Va a ir en la tabla
     * @param productID
     * @param quantity
     * @param freeProduct
     * @return 
     */
    public double addNFreeProducts(int productID, int quantity, int freeProduct){
        this.specialPrice = this.getPrice(quantity);
        if(quantity>=freeProduct){
            ProductFactory productFactory = new ProductFactory();
            int productAmount;
            if(quantity==1){
                quantity = 2; freeProduct = 1; productAmount = 2;
            }else{
                productAmount = quantity + freeProduct;
            }
            //Buy quantity. Get freeProduct free! Va a ir en la tabla
            this.setName(quantity + "X" + freeProduct);
            this.setDescription("Buy " + quantity + " " + this.productName() + ". Get " + freeProduct + " " + this.productName() + " free!");
            for(int n = 0; n < productAmount; n++){
                promoProducts.add(productFactory.create(productID));
            }
            allThePromos.add(promoProducts);
            for(Collection<Product> promos: allThePromos){
                    System.out.println("---------------------------");
                for(Product p: promos){
                    System.out.println(p.getName());
                }
            }
                    System.out.println(promoProducts.size());
                    System.out.println(promoProducts);
                System.out.println(this.promoProducts.size());
                return specialPrice;
        }else{
            System.out.println("You can't offer more free product than bought product!");
            return 0;
        }
    }    
   
}


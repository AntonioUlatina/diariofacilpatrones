/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu.Manager;

import Carritos.Carrito;
import Catalogos.Catalogo;
import Cliente.Client;
import Cliente.User;
import DAO.Category.CategoryController;
import DAO.Combo.ComboController;
import DAO.Deal.DealController;
import DAO.Product.ProductController;
import DAO.Provider.ProviderController;
import DAO.User.UserController;
import Decorator.ComboDecorator;
import Decorator.GetNFreeDecorator;
import Observer.Product;
import Observer.Provider;
import diariofacil.Store.Store;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class MenuManager {

    UserController uc = UserController.getInstance();
    ProductController pc = ProductController.getInstance();
    ProviderController provc = ProviderController.getInstance();
    DealController dc = DealController.getInstance();
    CategoryController catc = CategoryController.getInstance();
    ComboController cc = ComboController.getInstance();

    User user;
    Carrito carrito;
    Scanner sc = new Scanner(System.in);
    public static byte optionMaintenance;
    public static boolean continuar;
    public static String nombre;
    public static double precio;
    public static String categoria;
    public static int cantidad;
    public static int descuento;
    public static Product product;
    public static Provider provider;

    public void menuManager() {
        System.out.printf(" 1. Maintenance Clients\n 2. Maintenance Providers\n 3. Maintenance Product\n 4. Maintenance Combos\n 5. Maintenance Deals\n 6. Log out\n 7. Exit\n\n");

        Scanner sc = new Scanner(System.in);
        System.out.print("Choose an option: ");
        optionMaintenance = sc.nextByte();
        System.out.print("\n\n");
        switch (optionMaintenance) {
            case 1:
                maintenanceClients();
                break;
            case 2:
                maintenanceProviders();
                break;
            case 3:
                maintenanceProducts();
                break;
            case 4:
                maintenanceCombos();
                break;
            case 5:
                maintenanceDeals();
                break;
            case 6:
                new Store().welcome();
                break;
            case 7:
                System.out.println("Gracias por utilizar el inventario.");
                System.exit(0);
            default:
                menuManager();
        }
                menuManager();
    }

    private User createClient() {
        String name, lastName, username;
        User client = new Client();
        System.out.println("Enter the client's name: ");
        name = sc.next();
        client.setName(name);
        System.out.println("Enter the client's last name: ");
        lastName = sc.next();
        client.setLastName(lastName);
        System.out.println("Enter the client's username: ");
        username = sc.next();
        client.setUsername(username);
        return client;
    }

    private void maintenanceClients() {
        byte option;
        System.out.println("1. View Clients\n 2. Add Client\n 3. Delete Client\n 4. Back\n 5. Exit\n\n");
        System.out.print("Enter an option number: ");
        System.out.println();
        option = sc.nextByte();
        System.out.println("");
        switch (option) {
            case 1:
                uc.viewUsers();
                break;
            case 2:
                uc.create(createClient());
                break;
            case 3:
                uc.viewUsers();
                System.out.print("Insert the ID of the client to delete: ");
                int clientID = sc.nextInt();
                uc.delete(clientID);
                break;
            case 4:
                menuManager();
                break;
            case 5:
                System.exit(0);
                break;
            default:
                maintenanceClients();
                break;
        }
        maintenanceClients();
    }

    public void maintenanceCombos() {
        System.out.print("1. Crear combo\n2. Actualizar combo\n3. Eliminar combo\n4. Ver combos\n5. Back al menu principal\n6. Salir\n\n");
        byte option;
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite la option: ");
        option = sc.nextByte();
        System.out.print("\n\n");
        switch (option) {
            case 1:
                createCombo();
                break;
            case 2:
                updateCombo();
                break;
            case 3:
                deleteCombo();
                break;
            case 4:
                listCombos();
                break;
            case 5:
                menuManager();
                break;
            case 6:
                System.out.println("Gracias por utilizar el inventario.");
                System.exit(0);
            default:
                maintenanceCombos();
        }
        maintenanceCombos();
    }

    private boolean confirmContinue(String msg) {
        while (true) {
            System.out.println(msg + " Yes or No?");
            String nextLine = sc.nextLine();
            if ((nextLine.equalsIgnoreCase("yes")) || (nextLine.equalsIgnoreCase("y")) || (nextLine.equalsIgnoreCase("si"))) {
                return true;
            } else if ((nextLine.equalsIgnoreCase("no")) || (nextLine.equalsIgnoreCase("n"))) {
                return false;
            }
        }
    }

    private void listCombos() {
        cc.viewCombos(new ComboDecorator(new Product(), new ArrayList<Product>()));
    }

    private void createCombo() {
        Product product = new Product();
        List<Product> products = new ArrayList<>();

        while (true) {
            int id;

            new Catalogo();
            System.out.println("Enter the ID of the product you wish to add to the combo.");
            try {
                id = sc.nextInt();
                sc.nextLine();
                if (pc.doesProductExist(id)) {
                    products.add(pc.giveProduct(id));
                    if (!confirmContinue("Continue adding products to the combo?")) {
                        break;
                    }
                } else {
                    System.out.println("No product with ID " + id + " exists.");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Invalid ID");
                sc.nextLine();
            }
        }

        System.out.print("Enter the combo's name: ");
        product.setName(sc.nextLine());

        cc.create(new ComboDecorator(product, products));

        System.out.println("Combo created!");
    }

    private void updateCombo() {
        ComboDecorator cd;

        while (true) {
            int id;

            listCombos();
            System.out.println("Enter the ID of the combo you wish to edit.");
            try {
                id = sc.nextInt();
                sc.nextLine();
                cd = cc.loadCombo(id);
                if (cd != null) {
                    break;
                } else {
                    System.out.println("No combo with ID " + id + " exists.");
                }

            } catch (InputMismatchException ex) {
                System.out.println("Invalid ID");
                sc.nextLine();
            }
        }

        if (confirmContinue("Do you want to add products to this combo?")) {
            while (true) {
                int id;

                new Catalogo();
                System.out.println("Enter the ID of the product you wish to add to the combo.");
                try {
                    id = sc.nextInt();
                    sc.nextLine();
                    if (pc.doesProductExist(id)) {
                        cd.getProducts().add(pc.giveProduct(id));
                        if (!confirmContinue("Continue adding products to the combo?")) {
                            break;
                        }
                    } else {
                        System.out.println("No product with ID " + id + " exists.");
                    }
                } catch (InputMismatchException ex) {
                    System.out.println("Invalid ID");
                    sc.nextLine();
                }
            }
        } else if ((cd.getProducts().size() != 0) && (confirmContinue("Do you want to remove products from this combo?"))) {
            while (true) {
                int id;
                int counter = 1;

                System.out.println("Products: ");

                for (Product p : cd.getProducts()) {
                    System.out.println(counter + ". " + p.getName());
                    counter++;
                }

                System.out.println("Enter the ID of the product you wish to remove from the combo.");

                try {
                    id = sc.nextInt();
                    sc.nextLine();
                    if ((id >= 1) && (id <= cd.getProducts().size())) {
                        cd.getProducts().remove(id - 1);
                        if ((cd.getProducts().size() == 0) || (!confirmContinue("Continue removing products from the combo?"))) {
                            break;
                        }
                    } else {
                        System.out.println("No product with ID " + id + " exists.");
                    }
                } catch (InputMismatchException ex) {
                    System.out.println("Invalid ID");
                    sc.nextLine();
                }
            }
        }

        if (confirmContinue("Do you wish to change the name of this combo?")) {
            System.out.print("Enter the combo's new name: ");
            cd.setName(sc.nextLine());
        }

        cc.update(cd);

        System.out.println("Combo updated!");
    }

    private void deleteCombo() {
        int id;

        while (true) {
            listCombos();
            System.out.println("Enter the ID of the combo you wish to delete.");
            try {
                id = sc.nextInt();
                break;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid ID");
                sc.nextLine();
            }
        }

        cc.delete(id);

        System.out.println("Combo deleted!");
    }

    private Product createProduct() {
        String name;
        int quantity, category;
        double price;
        Product product = new Product();
        System.out.println("Enter the product's name: ");
        name = sc.next();
        product.setName(name);
        System.out.println("Enter the product amount: ");
        quantity = sc.nextInt();
        product.setQuantity(quantity);
        System.out.println("Enter the product's price: ");
        price = sc.nextDouble();
        product.setPrice(price);
        catc.viewCategories();
        System.out.println("Enter the ID for the product's category: ");
        category = sc.nextInt();
        product.setCategory(category);
        return product;
    }

    public void modifyProduct() {
        System.out.println("--- * Modify Product* ---\n");
        new Catalogo();
        System.out.println("Type the ID of the product you want to edit: ");
        int id = sc.nextInt();
        product = pc.giveProduct(id);

        System.out.println("1. Name.\n2. Quantity." + "\n3. Price.\n4. CategoryID.\n5. Back.\n");
        System.out.println("Type the choice number: ");
        int option = sc.nextInt();

        switch (option) {
            case 1:
                System.out.println("Type the name: ");
                product.setName(sc.next());
                pc.update(product);
                break;
            case 2:
                System.out.println("Type the new quantity: ");
                product.setQuantity(sc.nextInt());
                pc.update(product);
                break;
            case 3:
                System.out.println("Type the new price: ");
                product.setPrice(sc.nextDouble());
                pc.update(product);
                break;
            case 4:
                catc.viewCategories();
                System.out.println("Type the new category ID: ");
                product.setCategory(sc.nextInt());
                pc.update(product);
                break;
            case 5:
                maintenanceProducts();
                break;
            default:
                modifyProduct();
        }
                modifyProduct();
    }

    public void maintenanceProducts() {
        System.out.print("1. View Products\n2. Modify products\n3. Delete products\n4. Add product\n5. Back\n6. Exit\n\n");
        byte option;
        Scanner sc = new Scanner(System.in);
        System.out.print("Choose an option: ");
        option = sc.nextByte();
        System.out.print("\n\n");

        switch (option) {
            case 1:
                new Catalogo();
                break;
            case 2:
                modifyProduct();
                break;
            case 3:
                new Catalogo();
                System.out.print("Insert the ID for the product you want to delete: ");
                int id = sc.nextInt();
                pc.delete(id);
                break;
            case 4:
                pc.create(createProduct());
                break;
            case 5:
                menuManager();
                break;
            case 6:
                System.out.println("Gracias por utilizar el inventario.");
                System.exit(0);
            default:
                maintenanceProducts();
        }
        maintenanceProducts();
    }

    public GetNFreeDecorator createDeal() {
        new Catalogo();
        System.out.print("Type a productID to select a product: ");
        int id = sc.nextInt();
        int quantity, freeProduct;
        GetNFreeDecorator deal = new GetNFreeDecorator(pc.giveProduct(id));
        System.out.println("Enter the quantity of products to pay for: ");
        quantity = sc.nextInt();
        System.out.println("Enter the quantity of giveaway products: ");
        freeProduct = sc.nextInt();
        deal.addNFreeProducts(id, quantity, freeProduct);
        return deal;
    }

    public void maintenanceDeals() {

        System.out.print("1. View Deals\n2. Modify Deal\n3. Create Deal\n4. Delete deal\n5. Back\n6. Exit\n\n");
        byte option;
        System.out.print("Digite la option: ");
        option = sc.nextByte();
        switch (option) {
            case 1:
                dc.viewDeals();
                break;
            case 2:
//                dc.update(deal);
                break;
            case 3:
                dc.create(createDeal());
                break;
            case 4:
                dc.viewDeals();
                break;
            case 5:
                menuManager();
                break;
            case 6:
                System.out.println("Gracias por utilizar el inventario.");
                System.exit(0);
            default:
                maintenanceDeals();
        }
            maintenanceDeals();
    }

    public void modifyProvider() {
        provc.viewProviders();
        System.out.println("--- * Modify Provider * ---\n" + "Type the id of the provider you want to edit: ");
        int id = sc.nextInt();

        provider = provc.giveProvider(id);

        System.out.println("\n1. Name\n"
                + "2. Email" + "\n3. Phone number\n"
                + "4. Product Category" + "\n5. Back\n");
        int option = sc.nextInt();
        switch (option) {
            case 1:
                System.out.println("Type the name: ");
                provider.setCompany(sc.next());
                provc.update(provider);
            case 2:
                System.out.println("Type the email: ");
                provider.setEmail(sc.next());
                provc.update(provider);
            case 3:
                System.out.print("Type the new phone number: ");
                provider.setPhone(sc.next());
                provc.update(provider);
            case 4:
                System.out.println("Type the new category ID: ");
                provider.setCategory(sc.nextInt());
                provc.update(provider);
            case 5:
                maintenanceProviders();
            default:
                modifyProvider();
        }
    }

    public void modifyClient() {
        uc.viewUsers();
        System.out.println("--- * Modify Client * ---\n");
        System.out.println("Type the ID of the client: ");
        int id = sc.nextInt();
        User client = uc.giveClient(id);
        System.out.println("Enter the option to edit: ");
        System.out.println("1. Username.\n2. Name."
                + "\n3. Last Name.\n4. Back.");
        switch (sc.nextInt()) {
            case 1:
                System.out.print("Enter the new Username");
                client.setUsername(sc.next());
                uc.update(client);
                break;
            case 2:
                System.out.print("Enter your name");
                client.setName(sc.next());
                uc.update(client);
                break;
            case 3:
                System.out.println("Enter your last name");
                client.setLastName(sc.next());
                uc.update(client);
                break;
            case 4:
                maintenanceClients();
                break;
        }
    }

    private Provider createProvider() {
        String company, email, phone;
        int category;
        Provider provider = new Provider();
        System.out.println("Enter the provider's company name: ");
        company = sc.next();
        provider.setCompany(company);
        System.out.print("Enter the provider's email: ");
        email = sc.next();
        provider.setEmail(email);
        System.out.print("Enter the provider's phone number: ");
        phone = sc.next();
        provider.setPhone(phone);
        catc.viewCategories();
        System.out.println("Enter the ID for the provider's category: ");
        category = sc.nextInt();
        provider.setCategory(category);
        return provider;
    }

    /**
     * @see I need to unregister de provider for the product(s) before I deleted
     * it!!!!!!!!!!!!!
     * @see
     * @see Ive implemented the unregister inside of the ProviderController
     * @see
     */
    public void maintenanceProviders() {
        System.out.print("1. View Providers\n" + "2. Add Provider\n" + "3. Modify Provider\n" + "4. Delete Provider\n" + "5. Back\n" + "Digite una option:\n");
        int option = sc.nextInt();
        switch (option) {
            case 1:
                provc.viewProviders();
                break;
            case 2:
                provc.viewProviders();
                provc.create(createProvider());
                break;
            case 3:
                provc.update(provider);
                modifyProvider();
                break;
            case 4:
                provc.viewProviders();
                System.out.print("Insert the ID for the provider you want to delete: ");
                int id = sc.nextInt();
                provc.unregisterProvider(id);
                provc.delete(id);
                break;
            case 5:
                menuManager();
                break;
            default:
                maintenanceProviders();
        }
        maintenanceProviders();
    }

}

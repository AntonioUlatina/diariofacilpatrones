/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu.Client;

import Carritos.Carrito;
import Catalogos.Catalogo;
import Cliente.User;
import Common.SendEmail;
import static DAO.DAO.connect;
import DAO.Product.ProductController;
import Decorator.Item;
import Observer.Product;
import java.util.ArrayList;
import java.util.Scanner;
import diariofacil.Store.Store;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import Factory.ProductFactory;
import Factory.Receipt.Receipt;
import Factory.Receipt.ReceiptFactory;
import java.io.IOException;
import java.sql.ResultSet;

/**
 *
 * @author Administrator
 */
public class MenuClient {

    ProductController proc = ProductController.getInstance();
    List<Product> lstProductsOrden = new ArrayList<>();
    
    private void changePassword(String password) {
        PreparedStatement ps = null;
        String SQLpass = "SELECT pass FROM users WHERE id = ?;";
        String SQL = "UPDATE users SET pass = ? WHERE id = ?;";
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQLpass);
            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();{
                while(rs.next()){
                    if(!rs.getString("pass").equals(password)){
                        ps = conn.prepareStatement(SQL);
                        ps.setString(1, password);
                        ps.setInt(2, user.getId());
                    }else{
                        System.err.println("This password is already in use. Try a different password!");
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("Something went wrong. Password was not changed!");
            return;
        }
    }
    
    User user;
    Carrito carrito;
    Scanner sc = new Scanner(System.in);
    public byte optionMaintenance;
    public boolean continuar;
    public String nombre;
    public double precio;
    public String categoria;
    public int cantidad;
    public int descuento;
    public Product product;
    
     public void menuClient(){
//        System.out.printf("1. Check Catalogue\n 2. Add to cart\n 3. Crear orden de compra\n 4. Consultas\n 5. Log out\n 6. Exit\n\n");
        System.out.printf("1. Check Catalogue\n 2. Check cart\n 3. Change password\n 4. Log out\n 5. Exit\n\n");
        
        System.out.print("Digite la opcion: ");
        optionMaintenance = sc.nextByte();
        System.out.print("\n\n");
        switch (optionMaintenance) {
            case 1:
                new Catalogo();
                break;
            case 2:
                cartOptions();
                break;
            case 3:
                System.out. print("Enter your password: ");
                String password = sc.next();
                changePassword(password);
                break;
            case 4:
                new Store().welcome();
                break;
            case 5:
                System.out.println("Gracias por utilizar el inventario.");
                System.exit(0);
                break;
            default:
                menuClient();
        }
                menuClient();
    }
     
    // ANTONIO AQUI CAMBIOSSS ------------------------------------ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
    /**
     * @see Puede que el tener la lista de productos local en el metodo, de comportamientos indeseables
     */
    public void addToCart() {
        int id, itemID = 0, quantity;
        Product newProduct = new Product();
        new Catalogo();
        System.out.println("Press the enter key to continue...");
        try {
            while(System.in.available() == 0){}
        } catch (IOException ex) {
            ex.printStackTrace();
        }
            System.out.println("Insert the ID of the product");
            id = sc.nextInt();

            if (proc.doesProductExist(id)) {
            System.out.println("Insert the desired quantity");
            quantity = sc.nextInt();
                //Transferir a lista de productos en Carrito
                newProduct.decreaseQuantity(id, quantity);
                Product auxProduct = new ProductFactory().create(id);
                auxProduct.setQuantity(quantity);
                carrito.addItems(auxProduct, quantity);
                System.out.println("INSIDE ADD TO CART. HashMap size: " + carrito.getItems().size());
            }

            menuClient();
        }

    public Item getItem(int id){
        Item item = null;
//        for(Integer i: carrito.getItems().keySet()){
//            item = carrito.getItems().remove(i);
        item = (Item) carrito.getItems().toArray()[id-1];
        return item;
    }
    
    private void cartOptions() {
        int option, id;
        System.out.println("1. View Cart\n2. Add to Cart\n3. Remove from cart\n4. Recover product\n5. Make purchase\n6. Back");
        System.out.print("Type the option's number: ");
        option = sc.nextInt();
        switch(option){
            case 1:
//                if(!carrito.isEmpty()){
//                    System.out.println(carrito.toString());
//                }
                cartOptions();
                break;
            case 2:
//                if(!carrito.isEmpty()){
//                    System.out.println(carrito.cartProducts());
//                }
                addToCart();
                cartOptions();
                break;
            case 3:
                //show cart here
                System.out.println(carrito.toString());
                System.out.print("Type the id for the item you wish to remove: ");
                id = sc.nextInt();
                carrito.removeItem(getItem(id));
//                carrito.removeItem(item);
                break;
            case 4:
                System.out.println("Type the id for the item you wish to recover: ");
                id = sc.nextInt();
                carrito.recoverItem(getItem(id));
                //show memento hashmap here
                
//                carrito.recoverItem(sc.nextInt());
               break;
            case 5:
                Receipt receipt = new ReceiptFactory().getReceipt(carrito, "detailed");
                new SendEmail().sendReceiptToClient(receipt.toString(),user.getEmail());
                break;
            case 6:
                menuClient();
                break;
            default:
                cartOptions();
        }
        cartOptions();
    }

}
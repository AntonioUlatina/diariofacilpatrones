/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Carritos;

import Cliente.Client;
import Decorator.Item;
import Factory.ProductFactory;
import Observer.Product;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
/**
 *
 * @author Mateo Marin
 */
public class Carrito {
//    private Item item = new Item();
//    private Map<Integer,Item> items = new Hashtable();
    private Collection<Item> items = new ArrayList();
    private static Collection<Item> removedItems = new ArrayList();
    
    private int id;
    private Client client;
    private Date fecha;
    private double subtotal;

    {
        this.id = 0;
        this.subtotal = 0;
    }
    public Carrito() {
    }
   
    public Carrito(int id, Client client, Date fecha) {
        this.id = id;
        this.client = client;
        this.fecha = fecha;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public void addItems(Product product, int quantity) {
        this.getItems().add(new Item(++id, product, quantity));
//        this.items.put(id, new Item(++id, product, quantity));
    }
    
    public boolean isEmpty(){
        boolean flag = false;
        if(!this.getItems().isEmpty()){
            flag = true;
        }
        return flag;
    }

    /**
     * @deprecated 
     * @return 
     */
    public Map<Integer,Item> getItemsDeprecated() {
//        return items;
        return null;
    }
    
    public Collection<Item> getItems() {
        return items;
    }
    
    public static void addMemento(Item item)
    {
        removedItems.add(item);
//      this.removedItems.put(item.getId(), item); 
    }
    //elimina producto del memento
    public void getMemento(Item item)
    {
        items.add(item);
        removedItems.remove(item);
//      return this.removedItems.replace(item.getId(), item); 
    }
    
    public double getSubtotal(){
        for(Item item: items){
            subtotal += item.getProduct().getPrice() * item.getProduct().getQuantity();
        }
//         for (Integer key : items.keySet()) {
//             Product product = items.get(key);
//              subtotal += product.getPrice()*product.getQuantity();        
//        }
         return subtotal;        
    }
    
    public String cartProducts(){
        StringBuilder sb = new StringBuilder();
        for(Item item: items){
            Product product = item.getProduct();
            sb.append(product.getName() + " " + product.getPrice() + " " + product.getQuantity() + "\n");
        }
//        for(Integer key : items.keySet()) {
//            Product product = items.get(key);
//            sb.append(product.getName() + " " + product.getPrice() + " " + product.getQuantity() + "\n");
//        }
         return sb.toString();
    }
    
    public void removeItem(Item item){
        addMemento(item);
    }
    
    public void recoverItem(Item item){
        getMemento(item);
        //may have to switch the id manually
//        addItems(item.getId(), rc.getMemento(item));
    }
    
    public String toString() {
        double sub = 0;
        sub+=getSubtotal();
        String cart = cartProducts();
        return  "=============================================cart============================================"+
                "\nId: " + id 
                + "\nClient: " + client 
                + "\nFecha: " + fecha
                + "\nProducts: " + cart
                + "\nSubtotal: " + sub + "\n"
                +"===============================================================================================";
    }   
}

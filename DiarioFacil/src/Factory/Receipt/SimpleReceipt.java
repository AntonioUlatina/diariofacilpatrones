/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory.Receipt;

import Carritos.Carrito;


/**
 * 
 * @author Administrator
 */
public class SimpleReceipt implements Receipt {
    
    Carrito carrito;
    
    public SimpleReceipt(Carrito carrito) {
        this.carrito = carrito;
    }

    @Override
    public void print() {
        System.out.println("Carrito #" + carrito.getId());
        System.out.println("Cliente: " + carrito.getClient().getName() + " " + carrito.getClient().getLastName());
        System.out.println("Fecha: " + carrito.getFecha());
        System.out.println("Items: " + carrito.getItems().size());
        System.out.println("Total: " + carrito.getSubtotal());
    }
    
}

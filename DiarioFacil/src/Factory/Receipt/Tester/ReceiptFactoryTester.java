package Factory.Receipt.Tester;

import Carritos.Carrito;
import Cliente.Client;
import Decorator.Item;
import Factory.Receipt.Receipt;
import Factory.Receipt.ReceiptFactory;
import Observer.Product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author Administrator
 */
public class ReceiptFactoryTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Client cliente = new Client("John", "Smith", "jsmith");
        Carrito carrito = new Carrito();
        
        Product p1 =  new Product("Angus", 69, 420, 0);
        Product p2 =  new Product("Leche", 3, 200, 0);
        Product p3 =  new Product("Pato", 2, 300, 0);
        
        carrito.getItems().add(new Item(1,p1,3));
        carrito.getItems().add(new Item(2,p2,5));
        carrito.getItems().add(new Item(3,p3,90));
//        carrito.getItems().put(1,new Item(1,p1,3));
//        carrito.getItems().put(2,new Item(2,p2,5));
//        carrito.getItems().put(3,new Item(3,p3,90));
        
        ReceiptFactory factory = new ReceiptFactory();
        
        Receipt simple = factory.getReceipt(carrito, "SIMPLE");
        Receipt detailed = factory.getReceipt(carrito, "DETAILED");
        
        System.out.println("########## START Simple START ##########");
//        simple.print();
        System.out.println("########## END Simple END ##########");
        System.out.println("########## START Detailed START ##########");
        detailed.print();
        System.out.println("########## END Detailed END ##########");
    }

}

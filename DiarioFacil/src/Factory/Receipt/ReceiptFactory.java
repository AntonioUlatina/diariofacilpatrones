/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory.Receipt;

import Carritos.Carrito;

/**
 * 
 * @author Administrator
 */
public class ReceiptFactory {

    public Receipt getReceipt(Carrito carrito, String receiptType) {

        if (receiptType.equalsIgnoreCase("SIMPLE")) {
            return new SimpleReceipt(carrito);

        } else if (receiptType.equalsIgnoreCase("DETAILED")) {
            return new DetailedReceipt(carrito);
        }

        return null;
    }

}

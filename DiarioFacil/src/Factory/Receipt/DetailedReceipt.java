/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory.Receipt;

import Carritos.Carrito;
import Decorator.Item;
import Observer.Product;

/**
 * 
 * @author Administrator
 */
public class DetailedReceipt implements Receipt {

    Carrito carrito;

    public DetailedReceipt(Carrito carrito) {
        this.carrito = carrito;
    }

    @Override
    public void print() {
        System.out.println("Carrito #" + carrito.getId());
//        System.out.println("Client: " + carrito.getClient().getName() + " " + carrito.getClient().getLastName());
        System.out.println("Fecha: " + carrito.getFecha());
        System.out.println("Items: " + carrito.getItems().size());
        System.out.println("--------------------------------------------------");
//      for(Integer key : carrito.getItems().keySet()) {
//            Product p = carrito.getItems().get(key);
        for(Item item: carrito.getItems()){
            Product p = item.getProduct();
            System.out.println("Producto: " + p.getName() + " (" + p.getPrice() + ")" + " x" + p.getQuantity() + " " + p.getPrice() * p.getQuantity());
        }
        System.out.println("--------------------------------------------------");
        System.out.println("Total: " + carrito.getSubtotal());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Carrito #").append(carrito.getId()).append("\nClient: ").append(carrito.getClient().getName()).append(" ").append(carrito.getClient().getLastName());
        sb.append("\nFecha: ").append(carrito.getFecha()).append("Items: ").append(carrito.getItems().size());
        sb.append("\n--------------------------------------------------\n");
//        for(Integer key : carrito.getItems().keySet()) {
//            Product p = carrito.getItems().get(key);
        for(Item item: carrito.getItems()){
            Product p = item.getProduct();
            sb.append("Producto: ").append(p.getName()).append(" (").append(p.getPrice()).append(") x").append(p.getQuantity()).append(" ").append(p.getPrice() * p.getQuantity());
        };
        sb.append("\n--------------------------------------------------\n");
        sb.append("Total: ").append(carrito.getSubtotal());
        return sb.toString();
    }
}

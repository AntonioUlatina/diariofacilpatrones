/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

/**
 *
 * @author Pavilion
 */
import static DAO.DAO.connect;
import Observer.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @see Fabrica para productos
 * @author Administrator
 */
public class ProductFactory implements IFactory{
    
    /**
     * @return Product
     * @param productID
     * @see  
     */
    public Product create(int id){
        String SQL = "SELECT * FROM products WHERE id = ?;";
        PreparedStatement ps = null;
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return new Product(rs.getString("name"), rs.getInt("quantity"), rs.getDouble("price"), rs.getInt("category"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}

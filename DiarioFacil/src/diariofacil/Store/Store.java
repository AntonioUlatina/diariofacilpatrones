/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diariofacil.Store;

import DAO.User.UserController;
import Observer.Product;
import Carritos.Carrito;
import Cliente.Client;
import Cliente.User;
import Menu.Client.MenuClient;
import Menu.Manager.MenuManager;
import Strategy.loginBehaviour.ILogin;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 *
 * @author Administrator
 */

public class Store { 
    UserController uc = UserController.getInstance();
    User user;
    Carrito carrito;
    Scanner sc = new Scanner(System.in);
    public static byte optionMaintenance;
    public static boolean continuar;
    public static String nombre;
    public static double precio;
    public static String categoria;
    public static int cantidad;
    public static int descuento;
    public static Product product;
    {
        carrito = new Carrito();
    }
    /**
     * @param args the command line arguments
     */
    public void welcome(){
        int choice;
        System.out.println("Welcome to Diario Facil \n\n 1. Login \n 2. Sign Up \n 3. Exit\n");
        System.out.print("Please enter an option: ");
        choice = sc.nextInt();
        System.out.println();
        try{
            switch(choice){
                case 1:
                    loginMenu();
                    break;
                case 2:
                    uc.registerClient(registerClient());
                    break;
                case 3:
                    System.exit(0);
                default:
                    welcome();
                    break;
            }
        }catch(InputMismatchException|ClassCastException ex){
            welcome();
            ex.getMessage();
            System.err.println("Invalid value");
        }catch(Exception ex){
            System.err.println("Critical error, restarting program!");
            System.out.println("\n\n");
            welcome();
        }
                welcome();
    }
    
    public void loginMenu(){
        System.out.print("Insert your username: ");
        String username = sc.next();
        user = ILogin.loginSQL(username);
        System.out.println();
        if(user!=null){
                user.welcomeUser();
                if(user.getLoginType().toString().equalsIgnoreCase("Manager")){
                    new MenuManager().menuManager();
                }else{
                    new MenuClient().menuClient();
                }
//            }
        }else{
            System.out.println("Error critico\n");
            welcome();
        }
    }
   

    public Carrito getCarrito() {
        return carrito;
    }
    
    private User registerClient() {
        System.out.println("We appreciate your business. We'll take on a brief journey through our registration process.\n");
        String name, lastName, username, email, password;
        User client = new Client();
        System.out.println("Enter your first name: ");
        name = sc.next();
        client.setName(name);
        System.out.println("Enter your last name: ");
        lastName = sc.next();
        client.setLastName(lastName);
        System.out.println("Enter your email: ");
        email = sc.next();
        client.setEmail(email);
        System.out.println("Enter your username: ");
        username = sc.next();
        client.setUsername(username);
        System.out.println("Enter your password: ");
        password = sc.next();
        client.setPass(password);
        return client;
    }
    
    public static void main(String[] args) {
        Store s = new Store();
        s.welcome();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

/**
 *
 * @author Pavilion
 */
public class Manager extends User {
    
    {
        this.setLoginType(new Strategy.loginBehaviour.ManagerLogin());
    }

    public static Manager managerInstance = null;
    
    public static Manager getInstance(){
        if(managerInstance == null){
            managerInstance = new Manager();
        }
        return managerInstance;
    }
    
    public static Manager getInstance(String user, String pass){
        if(managerInstance == null){
            managerInstance = new Manager(user, pass);
        }
        return managerInstance;
    }
    
    public static Manager getInstance(int id, String name, String lastName, String user, String pass){
        if(managerInstance == null){
            managerInstance = new Manager(id, name, lastName, user, pass);
        }
        return managerInstance;
    }
    
    private Manager(){
        super();
    }
    
    private Manager(String user, String pass) {
        super(user, pass);
    }
    
    private Manager(int id, String name, String lastName, String user, String pass) {
        super(id, name, lastName, user, pass);
    }
    
}

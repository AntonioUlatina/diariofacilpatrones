/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

/**
 *
 * @author Pavilion
 */
public class Client extends User {

    private int id;
//    private String user;
    private String name;
    private String lastName;
    private String email;
    
    {
        this.setLoginType(new Strategy.loginBehaviour.ClientLogin());
    }

    public Client(){
        super();
    }
    
    public Client(String user, String pass) {
        super(user, pass);
    }
    
    public Client(String name, String lastName, String user) {
        super(name, lastName, user);
        this.setName(name);
        this.setUsername(user);
        this.setLastName(lastName);
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + id + ", name=" + name + ", lastName=" + lastName + '}';
    }
    
    public Client(int id, String name, String lastName, String user, String pass) {
        //base de datos int activo, si es 0, bienvenido esta es tu primera vez, por favor ingresa tu contrasena. Te vamos a pedir ingresar otra vez.
        //Si activo es 1, pedir contrasena.
        super(id, name, lastName, user, pass);
        this.setLoginType(new Strategy.loginBehaviour.ClientLogin());
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getEmail() {
        return email;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Strategy.loginBehaviour.ILogin;

/**
 *
 * @author Pavilion
 */
public abstract class User {
    
    private String username;
    private String pass;
    private int id;
    private String name;
    private String lastName;
    private ILogin loginType;
    private String email;

    {
        email = "";
    }
    public User(){}

    public User(String user, String pass) {
        this.username = user;
        this.pass = pass;
    }

    public User(int id, String name, String lastName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
    }
    
    public User(String name, String lastName, String user) {
        this.username = user;
        this.name = name;
        this.lastName = lastName;
    }
    
    public User(int id, String name, String lastName, String user, String pass) {
        this.username = user;
        this.pass = pass;
        this.id = id;
        this.name = name;
        this.lastName = lastName;
    }
    
    public void welcomeUser(){
        if(this instanceof Manager)
            System.out.printf("Welcome, %s \n\n", this.getUsername());
        else
            System.out.printf("Welcome, %s %s \n\n", this.getName(), this.getLastName());
    }
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public ILogin getLoginType() {
        return loginType;
    }

    public void setLoginType(ILogin loginType) {
        this.loginType = loginType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Deal;

import Decorator.GetNFreeDecorator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class DealController {
    
     public static DealController dealControllerInstance = null;
     
     private ViewDeal view = new ViewDeal();

    
    public static DealController getInstance() {
        if(dealControllerInstance == null){
            dealControllerInstance = new DealController();
        }
        return dealControllerInstance;
    }
    private DealController() {
    }

    //llama al DAO para guardar un user
    public void create(GetNFreeDecorator deal) {
        IDealDAO dao = new DealDAOImpl();
        dao.create(deal);
    }

    //llama al DAO para actualizar un user
    public void update(GetNFreeDecorator deal) {
        IDealDAO dao = new DealDAOImpl();
        dao.update(deal);
    }

    //llama al DAO para eliminar un user
    public void delete(GetNFreeDecorator deal) {
        IDealDAO dao = new DealDAOImpl();
        dao.delete(deal);
    }

    public void viewDeals() {
        new DealDAOImpl();
    }
    
}

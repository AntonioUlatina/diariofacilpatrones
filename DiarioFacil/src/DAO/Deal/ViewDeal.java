/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Deal;

import Decorator.GetNFreeDecorator;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ViewDeal {
    
    public void viewDeal(GetNFreeDecorator deal) {
		System.out.println("Datos del User: " + deal);
	}
	
	public void viewDeals(List<GetNFreeDecorator> deals) {
		for (GetNFreeDecorator deal : deals) {
			System.out.println("Datos del User: " + deal);
		}		
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Deal;

import static DAO.DAO.connect;
import Decorator.GetNFreeDecorator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class DealDAOImpl implements IDealDAO {

    {
        read();
    }
    
    public DealDAOImpl(){    
    }
    
    public boolean create(GetNFreeDecorator deal) {
        boolean created = false;

        PreparedStatement ps = null;

        String sql = "INSERT INTO deals (name,description,price,quantityProduct,productID) VALUES (?,?,?,?,?);";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, deal.getName());
            ps.setString(2, deal.getDescription());
            ps.setDouble(3, deal.getSpecialPrice());
            ps.setInt(4, deal.getQuantity());
//            ps.setDouble(3, deal.getPrice(deal.productQuantity()));
//            ps.setInt(4, deal.productQuantity());
            ps.setInt(5, deal.product.getCategory());
            ps.executeUpdate();
            created = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase DealDAOImpl, método registrar");
            e.printStackTrace();
        }
        return created;
    }

    public void read() {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM deals ORDER BY id;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery(sql);
            while (rs.next()) {
                System.out.println("Deal: " + rs.getString("name") + " Description: " + rs.getString("description")
                + " Price: " + rs.getDouble("price"));
            }
        } catch (SQLException e) {
            System.out.println("Error: Clase DealDAOImpl, método obtener");
            e.printStackTrace();
        }
    }

    public boolean update(GetNFreeDecorator deal) {
        PreparedStatement ps = null;

        boolean updated = false;

        String sql = "UPDATE deals SET nombreDeal=?,Precio=? WHERE idDeals=?;";
        try (Connection conn = connect();) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, deal.getName());
            ps.setDouble(2, deal.getPrice());
            ps.executeUpdate();
            updated = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase DealDAOImpl, método updated");
            e.printStackTrace();
        }
        return updated;
    }

    public boolean delete(GetNFreeDecorator deal) {
        PreparedStatement ps = null;

        boolean deleted = false;

        String sql = "DELETE FROM deals WHERE idDeals=?;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            deleted = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase DealDAOImpl, método deleted");
            e.printStackTrace();
        }
        return deleted;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.User;

/**
 *
 * @author Pavilion
 */
import Cliente.User;
import DAO.User.UserDAOImpl;
import DAO.User.IUserDAO;
import DAO.User.ViewUser;
import java.util.ArrayList;
import java.util.List;


public class UserController{
    
        public static UserController userControllerInstance = null;
	
        public static UserController getInstance(){
            if(userControllerInstance == null){
                userControllerInstance = new UserController();
            }
            return userControllerInstance;
        }
        
	private ViewUser view= new ViewUser();
	
	private UserController(){
	}
	
	//llama al DAO para guardar un user
	public void create(User user){
		IUserDAO dao= new UserDAOImpl();
		dao.create(user);
	}
	
	//llama al DAO para actualizar un user
	public void update(User user){
		IUserDAO dao= new  UserDAOImpl();
		dao.update(user);
	}
	
	//llama al DAO para eliminar un user
	public void delete(int id){
		IUserDAO dao= new UserDAOImpl();
		dao.delete(id);
	}
	
        public boolean newUser(User user){
            return new UserDAOImpl().isNewUser(user);
        }
        
        public void firstLogin(User user){
            new UserDAOImpl().firstLogin(user);
        }
	//llama al DAO para obtener todos los users y luego los muestra en la view
	public void viewUsers(){
		new UserDAOImpl();
	}
        
        public User giveClient(int id){
            return UserDAOImpl.returnClient(id);
        }
        
        public String registerClient(User user){
            return (UserDAOImpl.register(user)==true) ? "Client has been registered!" : "Error. Client could not be registered!";
        }
}

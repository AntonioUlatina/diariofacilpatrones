/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.User;

/**
 *
 * @author Pavilion
 */
import Cliente.User;
import java.util.List;

public class ViewUser {
	public void viewUser(User user) {
		System.out.println("Datos del User: " + user);
	}
	
	public void viewUsers(List<User> users) {
		for (User user : users) {
			System.out.println("Datos del User: " + user);
		}		
	}
}
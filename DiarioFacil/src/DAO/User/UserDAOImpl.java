/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.User;

/**
 *
 * @author Pavilion
 */
import Cliente.Client;
import Cliente.User;
import static DAO.DAO.connect;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserDAOImpl implements IUserDAO{	

    {
        read();
    }
    
    public UserDAOImpl() {
    }
    
        public static Client returnClient(int id){
            String SQL = "SELECT * FROM users WHERE id = ?;";
            PreparedStatement ps = null;
            try(Connection conn = connect()){
                ps = conn.prepareStatement(SQL);
                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
//                    String email = rs.getString("email");
                    if(rs.getString("email").contains(null))
                        return new Client(rs.getString("name"), rs.getString("lastname"), rs.getString("username"));
                    else{
                        Client client = new Client(rs.getString("name"), rs.getString("lastname"), rs.getString("username"));
                        client.setEmail(rs.getString("email"));
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return null;
        }
        
        /**
         * @deprecated It wasn't needed
         * @param user
         * @return 
         */
        public boolean isNewUser(User user){
            boolean hasNotLogedIn = false;
            PreparedStatement ps = null;
            ResultSet rs;
            String SQLFirstLogin = "SELECT * FROM diariofacil.users WHERE (user IS NULL OR pass IS NULL) AND id = ? GROUP BY id HAVING count(*) = 1;";
            
            try(Connection conn = connect()){
                ps = conn.prepareStatement(SQLFirstLogin);
                ps.setInt(1, user.getId());
                rs = ps.executeQuery();
                while(rs.next()){
                    hasNotLogedIn = true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return hasNotLogedIn;
        }
        
        /**
         * @deprecated Same for this one.
         * @param user 
         */
        public void firstLogin(User user){
            String SQLUpdate = "UPDATE users SET user = ?, pass = ? WHERE id = ?";
            PreparedStatement ps = null;
            try(Connection conn = connect()){
                ps = conn.prepareStatement(SQLUpdate);
                ps.setString(1, user.getUsername());
                ps.setString(1, user.getPass());
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
	@Override
	public boolean create(User user) {
		boolean created = false;
		
		PreparedStatement ps = null;
		
		String sql="INSERT INTO users (name,lastName,user,type) VALUES (?,?,?, 'client');";
		
		try(Connection conn=connect()) {			
			ps = conn.prepareStatement(sql);
                        ps.setString(1, user.getName());
                        ps.setString(2, user.getLastName());
                        ps.setString(3, user.getUsername());
			ps.executeUpdate();
			created = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase UserDAOImple, método registrar");
			e.printStackTrace();
		}
		return created;
	}
        
	public static boolean register(User user) {
		boolean created = false;
		
		PreparedStatement ps = null;
		
		String sql="INSERT INTO users (name,lastName,user,pass,type,email) VALUES (?,?,?,?, 'client', ?);";
		
		try(Connection conn=connect()) {			
			ps = conn.prepareStatement(sql);
                        ps.setString(1, user.getName());
                        ps.setString(2, user.getLastName());
                        ps.setString(3, user.getUsername());
                        ps.setString(4, user.getPass());
                        ps.setString(5, user.getEmail());
			ps.executeUpdate();
			created = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase UserDAOImple, método registrar");
			e.printStackTrace();
		}
		return created;
	}

	@Override
	public void read() {
            PreparedStatement ps = null;
            ResultSet rs = null;
            StringBuilder sb = new StringBuilder();

            String sql="SELECT * FROM users WHERE type = 'client' ORDER BY id;";

            try(Connection conn = connect()) {			
                    ps = conn.prepareStatement(sql);
                    rs = ps.executeQuery(sql);
                    while (rs.next()) {
                        sb.append("================\n");
                        sb.append("id" + "\t" + "name" + "\t" + "lastName" + "\t" + "\n");
                        sb.append(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getString("lastName") + "\n");
                        sb.append("================\n\n");
                    }
                    System.out.println(sb);
            } catch (SQLException e) {
                    System.out.println("Error: Clase UserDAOImple, método obtener");
                    e.printStackTrace();
            }
	}

	@Override
	public boolean update(User user) {
		PreparedStatement ps = null;
		
		boolean updated=false;
				
		String sql = "UPDATE users SET id=?, name=?, lastName=?, user=?, pass=? WHERE id=?;";
		try(Connection conn = connect();) {
			ps = conn.prepareStatement(sql);
                        ps.setInt(1, user.getId());
			ps.setString(2, user.getName());
			ps.setString(3, user.getLastName());
			ps.setString(4, user.getUsername());
                        ps.setString(5, user.getPass());
                        ps.setInt(6, user.getId());
			ps.executeUpdate();
			updated = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase UserDAOImple, método updated");
			e.printStackTrace();
		}		
		return updated;
	}

	@Override
	public boolean delete(int id) {
		PreparedStatement ps = null;
		
		boolean deleted = false;
				
		String sql = "DELETE FROM users WHERE id=?;";
                
		try(Connection conn = connect()){
			ps = conn.prepareStatement(sql);
                        ps.setInt(1, id);
			ps.executeUpdate();
			deleted = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase UserDAOImple, método deleted");
			e.printStackTrace();
		}		
		return deleted;
	}

}
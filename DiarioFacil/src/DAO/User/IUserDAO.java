/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.User;

/**
 *
 * @author Pavilion
 */
import Cliente.User;
import java.util.List;
 
public interface IUserDAO{	
	public void read();
	public boolean create(User user);
	public boolean update(User user);
	public boolean delete(int id);
}

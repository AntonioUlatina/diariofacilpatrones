/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.ConfigLoader;
import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.krb5.Credentials;

/**
 *
 * @author Pavilion
 */
public class DAO {
        
    final static String[] credentials = new ConfigLoader().readConfigDB();
    //  JDBC driver name
    final static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    //  Database URL
    final static String DB_URL = "jdbc:mysql://" + credentials[0]
                + "?useUnicode=true&useJDBCCompliantTimezoneShift=true"
                + "+&useLegacyDatetimeCode=false&serverTimezone=UTC"
                + "&autoReconnect=true&useSSL=false";
    //  Database credentials
    final static String USER = credentials[1];
    final static String PASS = credentials[2];
    
    private static final Connection getDataSourceConnection(String dbURL, String dbUser, String dbPass) throws SQLException{
        MysqlDataSource sqlDS = new MysqlDataSource();
        sqlDS.setUrl(dbURL);
        sqlDS.setUser(dbUser);
        sqlDS.setPassword(dbPass);
        Connection c = sqlDS.getConnection();
        return c;
    }
    
    public static final Connection connect(){
        try {
            return getDataSourceConnection(DB_URL, USER, PASS);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}

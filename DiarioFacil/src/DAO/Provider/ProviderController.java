/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Provider;

import static DAO.DAO.connect;
import Observer.Product;
import Observer.Provider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luis
 */
public class ProviderController {

    public static ProviderController providerControllerInstance = null;
    
    public static ProviderController getInstance() {
        if(providerControllerInstance == null){
            providerControllerInstance = new ProviderController();
        }
        return providerControllerInstance;
    }

    private ViewProvider view = new ViewProvider();

    private ProviderController() {
    }

    //llama al DAO para guardar un user
    public void create(Provider provider) {
        IProviderDAO dao = new ProviderDAOImpl();
        dao.create(provider);
    }

    //llama al DAO para actualizar un user
    public void update(Provider provider) {
        IProviderDAO dao = new ProviderDAOImpl();
        dao.update(provider);
    }

    //llama al DAO para eliminar un user
    public void delete(int id) {
        IProviderDAO dao = new ProviderDAOImpl();
        dao.delete(id);
    }

    //llama al DAO para obtener todos los users y luego los muestra en la view
    public void viewProviders() {
        view.viewProviders();
    }
    
    /**
     * @deprecated 
     * @param category
     * @return List<Provider>
     */
    public List<Provider> returnProviders(String category){
        List<Provider> providers = new ArrayList();
        String SQL = "SELECT prov.name, prov.email, prov.phone FROM providers AS prov, products AS pro, categories AS cat WHERE cat.name = ? AND prov.categoryProduct=pro.category AND prov.categoryProduct=cat.id;";
        PreparedStatement ps = null;
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setString(1, category);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                if(rs.getString("")!=null){
                }
                providers.add(new Provider(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getInt("categoryProduct")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return providers;
    }
    
    /**
     * @see La tabla provider no tiene representante todavia.
     * @see Y si falla, tambien halla que cambiarle el null por el string por defecto del proveedor.
     */
    public void unregisterProvider(int catID){
        Product product = new Product();
         String SQL = "SELECT prov.name, prov.email, prov.phone, prov.categoryProduct FROM providers AS prov, products AS pro WHERE prov.categoryProduct = ? AND prov.categoryProduct=pro.category;";
        PreparedStatement ps = null;
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, catID);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Provider provider;
                provider = new Provider(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getInt("categoryProduct"));
                product.unregister(provider);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @see If it did not work with , the fix would be to change el SELECT to SELECT *
     * @param catID
     * @return List<Provider>
     */
    public List<Provider> returnProviders(int catID){
        List<Provider> providers = new ArrayList();
        String SQL = "SELECT prov.name, prov.email, prov.phone, prov.category FROM providers AS prov, products AS pro, WHERE cat.name = ? AND prov.categoryProduct=pro.category;";
        PreparedStatement ps = null;
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, catID);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                if(rs.getString("")!=null){
                    providers.add(new Provider(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getInt("categoryProduct")));
                }
                providers.add(new Provider(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getInt("categoryProduct")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return providers;
    }
    
    public Provider giveProvider(int id){
            return ProviderDAOImpl.returnProvider(id);
        }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Provider;

import Observer.Provider;
import java.util.List;

/**
 *
 * @author Luis
 */
public interface IProviderDAO {
        public boolean create(Provider provider);
	public void read();
	public boolean update(Provider provider);
	public boolean delete(int id);
}

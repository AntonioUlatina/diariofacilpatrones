/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Provider;

import static DAO.DAO.connect;
import Observer.Provider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luis
 */
public class ProviderDAOImpl implements IProviderDAO{

    public ProviderDAOImpl(){
        read();
    }
    
    public static Provider returnProvider(int id){
        String SQL = "SELECT * FROM providers WHERE id = ?;";
        PreparedStatement ps = null;
        
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return new Provider(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getInt("categoryProduct"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public boolean create(Provider provider) {
        boolean created = false;

        PreparedStatement ps = null;

        String sql = "INSERT INTO providers (name,email,phone,categoryProduct) VALUES (?,?,?,?);";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, provider.getCompany());
            ps.setString(2, provider.getEmail());
            ps.setString(3, provider.getPhone());
            ps.setInt(4, provider.getCategory());
            ps.executeUpdate();
            created = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ProviderDAOImpl, método registrar");
            e.printStackTrace();
        }
        return created;
    }

    /**
     * @see La tabla provider no tiene representante todavia.
     * @see Y si falla, tambien halla que cambiarle el null por el string por defecto del proveedor.
     */
    public void read() {
        PreparedStatement ps = null;
        ResultSet rs = null;

        String SQL = "SELECT * FROM providers ORDER BY id;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery(SQL);
            while (rs.next()) {
                System.out.println("id: " + rs.getInt("id") + "Company: " + rs.getString("name") + "Email: " + rs.getString("email") + "Phone: " + rs.getString("phone") + "\n\n");
            }
        } catch (SQLException e) {
            System.out.println("Error: Clase ProviderDAOImple, método obtener");
            e.printStackTrace();
        }
    }

    public boolean update(Provider provider) {
        PreparedStatement ps = null;

        boolean updated = false;

        String sql = "UPDATE providers SET name=?,email=?,phone WHERE id=?;";
        try (Connection conn = connect();) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, provider.getCompany());
            ps.setString(2, provider.getEmail());
            ps.setString(3, provider.getPhone());
            ps.executeUpdate();
            updated = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ProviderDAOImple, método updated");
            e.printStackTrace();
        }
        return updated;
    }

    public boolean delete(int id) {
        PreparedStatement ps = null;

        boolean deleted = false;

        String sql = "DELETE FROM providers WHERE id = ?;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            deleted = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase providerDAOImple, método deleted");
            e.printStackTrace();
        }
        return deleted;
    }
}

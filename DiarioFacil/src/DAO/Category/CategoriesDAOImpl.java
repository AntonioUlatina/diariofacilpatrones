/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Category;

import static DAO.DAO.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Mateo Marin
 */
public class CategoriesDAOImpl implements ICategoriesDAO {
    
    CategoriesDAOImpl(){
        read();
    }
    
	public boolean create(String category) {
		boolean created = false;
		
		PreparedStatement ps = null;
		
		String sql="INSERT INTO categories (name) VALUES (?);";
		
		try(Connection conn=connect()) {			
			ps = conn.prepareStatement(sql);
                        ps.setString(1, category);
			ps.executeUpdate();
			created = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase CategoryDAOImpl, método registrar");
			e.printStackTrace();
		}
		return created;
	}

	
	public void read() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql="SELECT id, name FROM categories ORDER BY id;";
		
		try(Connection conn = connect()) {			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			while (rs.next()) {
				System.out.println("id: " + rs.getInt("id") + "name: " + rs.getString("name") + "\n\n");
			}
		} catch (SQLException e) {
			System.out.println("Error: Clase CategoryDAOImple, método obtener");
			e.printStackTrace();
		}
	}

	public boolean update(int id, String name) {
		PreparedStatement ps = null;
		
		boolean updated=false;
				
		String sql = "UPDATE categories SET name=? WHERE id=?;";
		try(Connection conn = connect()) {
			ps = conn.prepareStatement(sql);
			ps.setString(1, name);
                        ps.setInt(2, id);
			ps.executeUpdate();
			updated = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase CategoryDAOImple, método updated");
			e.printStackTrace();
		}		
		return updated;
	}

	public boolean delete(int id) {
		PreparedStatement ps = null;
		
		boolean deleted = false;
				
		String sql = "DELETE FROM categories WHERE id=?;";
                
		try(Connection conn = connect()){
			ps = conn.prepareStatement(sql);
                        ps.setInt(1, id);
			ps.executeUpdate();
			deleted = true;
		} catch (SQLException e) {
			System.out.println("Error: Clase UserDAOImple, método deleted");
			e.printStackTrace();
		}		
		return deleted;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Category;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class CategoryController {
    
    public static CategoryController categoryControllerInstance = null;
    
    private ViewCategory view = new ViewCategory();

    public static CategoryController getInstance() {
        if(categoryControllerInstance == null){
            categoryControllerInstance = new CategoryController();
        }
        return categoryControllerInstance;
    }

    private CategoryController(){
    }

    public void viewCategories(){
        view.viewCategories();
    }
    //llama al DAO para guardar un user
    public void create(String category){
            ICategoriesDAO dao =  new CategoriesDAOImpl();
            dao.create(category);
    }

    //llama al DAO para actualizar un user
    public void update(int id, String name){
            ICategoriesDAO dao = new  CategoriesDAOImpl();
            dao.update(id, name);
    }

    //llama al DAO para eliminar un user
    public void delete(int id){
            ICategoriesDAO dao = new  CategoriesDAOImpl();
            dao.delete(id);
    }

}

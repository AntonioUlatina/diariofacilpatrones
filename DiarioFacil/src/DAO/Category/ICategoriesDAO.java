/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Category;

/**
 *
 * @author Mateo Marin
 */
public interface ICategoriesDAO {
    public boolean create(String category);
	public void read();
	public boolean update(int id, String name);
	public boolean delete(int id);
}

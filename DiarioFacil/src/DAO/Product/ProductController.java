/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Product;

import Observer.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ProductController {

    public static ProductController prouctControllerInstance = null;
    
    public static ProductController getInstance() {
        if(prouctControllerInstance == null){
            prouctControllerInstance = new ProductController();
        }
        return prouctControllerInstance;
    }
    
    private ViewProduct view = new ViewProduct();
	
	private ProductController(){
	}
	
	//llama al DAO para guardar un product
	public void create(Product product){
            new ProductDAOImpl().create(product);
	}
	
	//llama al DAO para actualizar un product
	public void update(Product product){
            new ProductDAOImpl().update(product);
	}
	
	//llama al DAO para eliminar un product
	public void delete(int id){
            new ProductDAOImpl().delete(id);
	}
	
        public boolean doesProductExist(int id) {
            return new ProductDAOImpl().checkProducts(id);
        }
        
        public static void storeNewSupplies(Product product){
            new ProductDAOImpl().increaseSupplies(product);
        }
        
        public Product giveProduct(int id){
            return ProductDAOImpl.returnProduct(id);
        }
	//llama al DAO para obtener todos los users y luego los muestra en la view
}

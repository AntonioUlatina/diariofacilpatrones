/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Product;

import Observer.Product;

/**
 *
 * @author Mateo Marin
 */
public interface IProductDAO {
    public boolean create(Product product);
	public boolean update(Product product);
	public boolean delete(int id);
}

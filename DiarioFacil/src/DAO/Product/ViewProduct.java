/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Product;


import Observer.Product;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ViewProduct {
    
    public void viewProduct(Product product) {
		System.out.println("Datos del User: " + product);
	}
	
	public void viewProducts(List<Product> products) {
		for (Product product : products) {
			System.out.println("Datos del User: " + product);
		}		
	}
}

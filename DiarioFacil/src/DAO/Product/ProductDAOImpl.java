/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Product;

import Cliente.User;
import static DAO.DAO.connect;
import Observer.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateo Marin
 */
public class ProductDAOImpl implements IProductDAO {
    
    public static Product returnProduct(int id){
        String SQL = "SELECT * FROM products WHERE id = ?;";
        PreparedStatement ps = null;
        
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return new Product(rs.getString("name"), rs.getInt("quantity"), rs.getDouble("price"), rs.getInt("category"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Product viewProductsByCategory(int option){
        PreparedStatement ps = null;
        
        String SQL = "SELECT pro.id, pro.name, pro.price, pro.quantity FROM diariofacil.categories AS cat, diariofacil.products AS pro WHERE cat.id=? AND pro.category=cat.id;";
        try(Connection conn = connect()){
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, option);
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                System.out.println("id: " + rs.getInt("id") + "name: " + rs.getString("name") + "price: " + rs.getDouble("price") + "quantity: " + rs.getInt("quantity") + "\n\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean create(Product product) {
        boolean created = false;

        PreparedStatement ps = null;

        String sql = "INSERT INTO products (name,price,quantity,category) VALUES (?,?,?,?);";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, product.getName());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getQuantity());
            ps.setInt(4, product.getCategory());
            ps.executeUpdate();
            created = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDAOImple, método registrar");
            e.printStackTrace();
        }
        return created;
    }

    public boolean increaseSupplies(Product product) {
        String sqlID = "SELECT id FROM products where name = ?;";
        String sqlUp = "UPDATE products SET quantity=? WHERE id=?;";
        PreparedStatement ps = null;
        boolean updated = false;

        try (Connection conn = connect();) {
            ps = conn.prepareStatement(sqlID);
            ps.setString(1, product.getName());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                ps = conn.prepareStatement(sqlUp);
                ps.setInt(1, product.getQuantity());
                ps.setInt(2, rs.getInt("id"));
                ps.executeUpdate();
                updated = true;
            }
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDAOImple, método updated");
            e.printStackTrace();
        }
        return updated;
    }
    
    @Override
    public boolean update(Product product) {
        PreparedStatement ps = null;

        boolean updated = false;

        String sql = "UPDATE products SET name=?, price=?, quantity=? WHERE id=?;";
        try (Connection conn = connect();) {
            //Possible switch to modify separate stuff
            ps = conn.prepareStatement(sql);
            ps.setString(1, product.getName());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getQuantity());
            ps.executeUpdate();
            updated = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDAOImple, método updated");
            e.printStackTrace();
        }
        return updated;
    }

    @Override
    public boolean delete(int id) {
        PreparedStatement ps = null;

        boolean deleted = false;

        String sql = "DELETE FROM products WHERE id=?;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            deleted = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDAOImple, método deleted");
            e.printStackTrace();
        }
        return deleted;
    }
    
    /**
     * @see this could be made static
     * @param id
     * @return 
     */
    public boolean checkProducts(int id){
        String SQL = "SELECT * FROM products WHERE id=?;";
            PreparedStatement ps = null;
            try (Connection conn = connect()) {
                ps = conn.prepareStatement(SQL);
                ps.setInt(1, id);
                if(ps.execute()){
                    return true;
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        return false;
    }
}

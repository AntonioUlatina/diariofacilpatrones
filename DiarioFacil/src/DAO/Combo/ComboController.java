/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Combo;

import Decorator.ComboDecorator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ComboController {

    public static ComboController comboControllerInstance = null;

    private ViewCombo view = new ViewCombo();

    public static ComboController getInstance() {
        if (comboControllerInstance == null) {
            comboControllerInstance = new ComboController();
        }
        return comboControllerInstance;
    }

    private ComboController() {
    }

    //llama al DAO para guardar un user
    public void create(ComboDecorator combo) {
        IComboDAO dao = new ComboDAOImpl();
        dao.create(combo);
    }

    //llama al DAO para actualizar un user
    public void update(ComboDecorator combo) {
        IComboDAO dao = new ComboDAOImpl();
        dao.update(combo);
    }

    //llama al DAO para eliminar un user
    public void delete(int i) {
        IComboDAO dao = new ComboDAOImpl();
        dao.delete(i);
    }

    //llama al DAO para obtener todos los users y luego los muestra en la view
    public void viewCombos(ComboDecorator combo) {
        List<ComboDecorator> combos = new ArrayList<ComboDecorator>();
        IComboDAO dao = new ComboDAOImpl();
        combos = dao.read(combo);
        view.viewCombos(combos);
    }

    public ComboDecorator loadCombo(int i) {
        ComboDAOImpl dao = new ComboDAOImpl();
        return dao.load(i);
    }

}

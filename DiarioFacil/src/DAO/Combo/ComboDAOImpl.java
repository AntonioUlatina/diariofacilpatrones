/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Combo;

import static DAO.DAO.connect;
import Decorator.ComboDecorator;
import Observer.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ComboDAOImpl implements IComboDAO {

    @Override
    public boolean create(ComboDecorator combo) {
        boolean created = false;

        PreparedStatement ps = null;

        String sql = "INSERT INTO combos (name, price, products) VALUES (?,?,?);";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, combo.getName());
            ps.setDouble(2, combo.getPrice());
            ps.setString(3, productListToString(combo.getProducts()));
            ps.executeUpdate();
            created = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ComboDAOImpl, método registrar");
            e.printStackTrace();
        }
        return created;
    }

    @Override
    public List<ComboDecorator> read(ComboDecorator combo) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM combos ORDER BY id;";

        List<ComboDecorator> comboList = new ArrayList<ComboDecorator>();

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString("name"), 0, 0, 0);
                ComboDecorator cd = new ComboDecorator(p, stringToProductList(rs.getString("products")));
                cd.setId(rs.getInt("id"));
                comboList.add(cd);
            }
        } catch (SQLException e) {
            System.out.println("Error: Clase ComboDAOImple, método obtener");
            e.printStackTrace();
        }

        return comboList;
    }

    @Override
    public boolean update(ComboDecorator combo) {
        PreparedStatement ps = null;

        boolean updated = false;

        String sql = "UPDATE combos SET name=?,price=?,products=? WHERE id=?;";
        try (Connection conn = connect();) {
            ps = conn.prepareStatement(sql);
            ps.setString(1, combo.getName());
            ps.setDouble(2, combo.getPrice());
            ps.setString(3, productListToString(combo.getProducts()));
            ps.setInt(4, combo.getId());
            ps.executeUpdate();
            updated = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ComboDAOImple, método updated");
            e.printStackTrace();
        }
        return updated;
    }

    @Override
    public boolean delete(int i) {
        PreparedStatement ps = null;

        boolean deleted = false;

        String sql = "DELETE FROM combos WHERE id=?;";

        try (Connection conn = connect()) {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, i);
            ps.executeUpdate();
            deleted = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ComboDAOImple, método deleted");
            e.printStackTrace();
        }
        return deleted;
    }

    public ComboDecorator load(int i) {
        ComboDecorator u = new ComboDecorator(new Product(), new ArrayList<Product>());
        
        String sql = "SELECT * FROM combos WHERE id = ?;";

        try (Connection conn = connect();
                PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, i);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    u.setId(rs.getInt("id"));
                    u.setName(rs.getString("name"));
                    u.setPrice(rs.getDouble("price"));
                    u.setProducts(stringToProductList(rs.getString("products")));
                    return u;
                }
            }
        } catch (SQLException e) {
            System.out.println("Error: Clase ComboDAOImple, método obtener");
            e.printStackTrace();
        }

        return null;
    }

    private Product loadProduct(String name) {
        String SQL = "SELECT * FROM products WHERE name = ?;";

        try (Connection conn = connect();
                PreparedStatement ps = conn.prepareStatement(SQL)) {

            ps.setString(1, name);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return new Product(rs.getString("name"), rs.getInt("quantity"), rs.getDouble("price"), rs.getInt("category"));
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: Clase ComboDAOImple, método loadProduct");
            ex.printStackTrace();
        }
        return null;
    }

    private String productListToString(List<Product> products) {
        String list = "";

        for (Product p : products) {
            list += p.getName() + "\n";
        }

        return list;
    }

    private List<Product> stringToProductList(String products) {
        String[] productList = products.split("\n");
        List<Product> list = new ArrayList<>();

        for (String s : productList) {
            list.add(loadProduct(s));
        }

        return list;
    }

}

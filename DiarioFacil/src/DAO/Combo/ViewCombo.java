/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Combo;

import Decorator.ComboDecorator;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public class ViewCombo {
    
    public void viewCombo(ComboDecorator combo) {
		System.out.println("Datos del combo: " + combo);
	}
	
	public void viewCombos(List<ComboDecorator> combos) {
		for (ComboDecorator combo : combos) {
			System.out.println("Datos del combo: " + combo);
		}		
	}
}

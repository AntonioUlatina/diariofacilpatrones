/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Combo;

import Decorator.ComboDecorator;
import java.util.List;

/**
 *
 * @author Mateo Marin
 */
public interface IComboDAO {
    public boolean create(ComboDecorator combo);
	public List<ComboDecorator> read(ComboDecorator combo);
	public boolean update(ComboDecorator combo);
	public boolean delete(int i);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

import static DAO.DAO.connect;
import DAO.Product.ProductController;
import DAO.Provider.ProviderController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavilion
 */
public class Product implements IObservable{
    
    ProviderController provc = ProviderController.getInstance();
    List<Provider> providers = new ArrayList();
    private String name;
    protected int quantity;
    private double price;
    private int category;
    
    public Product() {
    }
    
    public Product(int quantity) {
        this.quantity = quantity;
    }

    public Product(String name, int quantity, double price, int category) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.category = category;
    }

    
    @Override
    public void register(Provider newObserver) {
        providers.addAll(provc.returnProviders(this.category));
    }

    @Override
    public void unregister(Provider observer) {
        providers.remove(observer);
    }

    @Override
    public void notifyObserver(int quantity) {
        for(Provider observer: providers){
            observer.update(this.name, quantity, observer.getEmail());
        }
    }
    
    /**
     * @deprecated 
     * @param quantity 
     */
    public void decreaseQuantity(int quantity){
        if(this.quantity>=quantity){
            System.out.printf("Cantidad anterior %d\n", this.quantity);
            System.out.printf("Gracias por tu compra de %d %s\n", quantity, this.name);
            this.quantity-=quantity;
            System.out.printf("Nueva cantidad %d\n", this.quantity);
            if(this.quantity<10)
                this.requestQuantity(quantity);
        }else{
            System.out.printf("There's not enough supply of %s\n", this.name);
        }
    }
    
    /**
     * 
     * @param productID
     * @param quantity 
     */
    public void decreaseQuantity(int productID, int quantity){
        String productQuantitySQL = "SELECT name, quantity FROM products WHERE id = ?;";
        String SQL = "UPDATE products SET quantity = quantity - ? WHERE id = ?;";
        try(Connection conn = connect()){
            PreparedStatement psSearch = conn.prepareStatement(productQuantitySQL);
            psSearch.setInt(1, productID);
            PreparedStatement psUpdate = conn.prepareStatement(SQL);
            psUpdate.setInt(1, quantity);
            psUpdate.setInt(2, productID);
            ResultSet rsQuantity = psSearch.executeQuery();
            while(rsQuantity.next()){
                this.name=rsQuantity.getString("name");
                this.quantity=rsQuantity.getInt("quantity");
                System.out.println("Before if: " + " DB " + this.quantity + " this.quantity: " + this.quantity);
                if(rsQuantity.getInt("quantity")>=quantity){
                    System.out.printf("Previous Quantity %d\n", this.quantity);
                    System.out.printf("Thak you for purchasing %d %s\n", quantity, this.name);
                    psUpdate.executeUpdate();
                    System.out.printf("New Quantity %d\n", this.quantity);
                }else{
                    System.out.printf("There's not enough supply of %s\n", this.name);
                }
                this.name=rsQuantity.getString("name");
                this.quantity-=quantity;
                System.out.println(this.name + " quantity: " + this.quantity);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex){
            ex.printStackTrace();
        }finally{
            if(this.quantity<10){
                this.requestQuantity(50);
//                this.requestQuantity(quantity);
            }
        }
    }
    
    public void requestQuantity(int newQuantity){
        if(getQuantity()<10){
            notifyObserver(newQuantity);
            this.quantity += newQuantity;
            ProductController.storeNewSupplies(this);
        }else
            System.out.println("La cantidad en inventario es mayor a la cantidad minima");
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public int getNewQuantity(int quantity){
        return this.quantity+=quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public double getPrice(){
        return this.price;
    }
    
    public double getPriceForSomething() {
        String SQL = "SELECT price FROM products WHERE name = ?;";
        ResultSet rs = null;
        try(Connection conn = connect()){
            PreparedStatement st = conn.prepareStatement(SQL);
            st.setString(1, this.getName());
            rs = st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    return rs.getDouble("price");
                }
            else
                System.err.println("This product does not exist!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return this.getPrice(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }

//     public static void receiveDelivery(int quantity) {
//        final String SQL = "UPDATE products SET quantity = ? WHERE id = (SELECT id FROM products WHERE name = ?)"; 
//        PreparedStatement ps = null;
//        try(Connection conn = connect()){
//            ps = conn.prepareStatement(SQL);
//            ps.setInt(1, new Leche().getNewQuantity(quantity));
//            ps.setInt(2, this.getName());
//            ps.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

}

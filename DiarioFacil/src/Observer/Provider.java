/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

import Common.SendEmail;

/**
 *
 * @author Mateo Marin
 */
public class Provider implements IObserver{

    private String companyName;
    private String email;
    private String phone;
    private int category;
    
    public Provider() {
    }

    public Provider(String companyName, String email, String phone, int category) {
        this.companyName = companyName;
        this.email = email;
        this.phone = phone;
        this.category = category;
    }

//    public void replenishDiarioFacil(int quantity){
//        Observer.Product.receiveDelivery(quantity);
//    }
    public String getCompany() {
        return companyName;
    }

    public void setCompany(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }
    
    @Override
    public String toString() {
        return "[" + companyName + "] " + " Email electrónico: " + email + " Tel: (506)" + phone + "\n";
    }

    @Override
    public void update(String name, int quantity, String... emails) {
        for(String email: emails)
            new SendEmail().sendOrderRequest("We are low on supplies. ", "\nWe require " + quantity + " units of " + name + ".", quantity, email);
//            new SendEmail().sendOrderRequest("We are low on supplies. ", "We got: " + this.product.getQuantity() + "\nWe require " + this.product.getName() + " units.", quantity, email);
        //Producto.setQuantity(); TODO
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

/**
 *
 * @author Pavilion
 */
public interface IObservable {
    
    public void register(Provider observer);
    public void unregister(Provider observer);
    public void notifyObserver(int quantity);
}

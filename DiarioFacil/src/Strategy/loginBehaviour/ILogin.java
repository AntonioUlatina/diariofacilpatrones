/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy.loginBehaviour;

import Cliente.Client;
import Cliente.Manager;
import Cliente.User;
import static DAO.DAO.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
    //public User login(String user, String pass);
 * @author Pavilion
 */
public interface ILogin{
    
    static void setPass(String user, String pass){
        String SQL = "UPDATE users SET pass = ? WHERE user = ?;";
        try (Connection conn = connect()) {
            PreparedStatement prepStmt = conn.prepareStatement(SQL);
            prepStmt.setString(1, pass);
            prepStmt.setString(2, user);
            prepStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ILogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    static User loginSQL(String user){
        Scanner sc = new Scanner(System.in);
        String SQL = "SELECT * FROM users WHERE user = ?;";
        User aux = null;
        try (Connection conn = connect()) {
                PreparedStatement prepStmt = conn.prepareStatement(SQL);
                prepStmt.setString(1, user);
                ResultSet rs = prepStmt.executeQuery();
            while (rs.next()) {
                    String rsUser = rs.getString("user");
                    String rsPass = rs.getString("pass");
                if (rsPass==null) {
                    String password;
                    System.out.println("It's a pleasure to make business with you. Before we got started we'd like to ask you for some information! :D");
                    System.out.print("\n Choose a password: ");
                    password = sc.next();
                    setPass(rsUser, password);
                    System.out.printf("\nWelcome to Diario Facil %s, we hope you'll enjoy your experience!\n\n", rs.getString("name"));
                    System.out.println("We will now require you to log in! We're redirecting you to the main menu.");
                    aux=null; //May be removed
                } else {
                    System.out.print("Insert your password: ");
                    String pass = sc.next();
                    if(rsPass.equals(pass)){
                        if(rs.getString("type").equalsIgnoreCase("manager")){
                            aux = Manager.getInstance(rsUser, rsPass);
                            aux.getLoginType();
                        }if(rs.getString("type").equalsIgnoreCase("client")){
                            aux = new Client(rs.getString("user"), rs.getString("name"), rs.getString("lastname"));
                            aux.getLoginType();
                        }
                    }
                }
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("No se pudo realizar la consulta de usuarios.");
        }
        return aux;
    }
    
}

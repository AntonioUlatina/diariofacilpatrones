/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy.loginBehaviour;

import static DAO.DAO.connect;
import Cliente.User;
import Cliente.Manager;
import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManagerLogin implements ILogin {
    
    /**
     * @Override
     * @deprecated 
     */
    public Manager login(String user, String pass) {
        User manager = Manager.getInstance(user, pass);
        return ((manager!=null && manager.getUsername().equals(user)) && (manager!=null && manager.getPass().equals(pass))) ? Manager.getInstance(user, pass) : null;
    }
    
    public User loginSQL(String user, String pass){
        System.out.println("MANAGER LOGIN");
        String SQL = "SELECT user, pass, type FROM users WHERE user = ? AND pass = ?;";
        User aux = null;
        try (Connection conn = connect()) {
                PreparedStatement prepStmt = conn.prepareStatement(SQL);
                prepStmt.setString(1, user);
                prepStmt.setString(2, pass);
                ResultSet rs = prepStmt.executeQuery();
            while (rs.next()) {
                String rsUser = rs.getString("user");
                String rsPass = rs.getString("pass");
                if(rs.getString("type").equalsIgnoreCase("manager")){
                    aux = Manager.getInstance(rsUser, rsPass);
                    aux.setLoginType(new ManagerLogin());
                }
            }
        }catch (SQLException ex){
            System.out.println("No se pudo realizar la consulta de usuarios.");
                    ex.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("No se pudo realizar la consulta de managers.");
        }
        return aux;
    }
    
    @Override
    public String toString(){
        return "Manager";
    }
    
}

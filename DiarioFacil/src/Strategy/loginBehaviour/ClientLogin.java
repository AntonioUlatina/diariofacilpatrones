/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy.loginBehaviour;

import Cliente.User;
import Cliente.Client;
import static DAO.DAO.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ClientLogin implements ILogin {

    /**
     * @deprecated 
     * @Override
     * */
    public Client login(String user, String pass) {
        User client = new Client(user, pass);
        return ((client!=null && client.getUsername().equals(user)) && (client!=null && client.getPass().equals(pass))) ? new Client(user, pass) : null;
    }

    public User loginSQL(String user, String pass) {
        System.out.println("CLIENT LOGIN");
        String SQL = "SELECT * FROM users WHERE user = ? AND pass = ?;";
        User aux = null;
        try (Connection conn = connect()){
        System.out.println("Estoy atrapado en el try");
                PreparedStatement prepStmt = conn.prepareStatement(SQL);
                prepStmt.setString(1, user);
                prepStmt.setString(2, pass);
                ResultSet rs = prepStmt.executeQuery();
                    System.out.println("Nombre: " + rs.getString("name"));
                    System.out.println("Type: " + rs.getString("type"));
            while (rs.next()) {
                if(rs.getString("type").equalsIgnoreCase("client")){
                    System.out.println("Yo si soy un cliente!");
                    aux = new Client(rs.getString("user"), rs.getString("name"), rs.getString("lastname"));
                }else
                    System.out.println("Yo no soy un cliente, yo soy un Manager");
            }   
        }catch (SQLException ex){
                    ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("No se pudo realizar la consulta de usuarios.");
        }
        return aux;
    }
    @Override
    public String toString(){
        return "Client";
    }

}
